<?php

namespace Drupal\steam_api;

/**
 * Steam API data.
 */
class Data {

  const STEAM_API_BASE_URL = 'https://api.steampowered.com';

  const STEAM_GET_API_KEY_URL = 'https://steamcommunity.com/dev/apikey';

}
