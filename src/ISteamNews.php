<?php

namespace Drupal\steam_api;

use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;

/**
 * ISteamNews API call.
 */
class ISteamNews extends ISteamApiBase implements ISteamNewsInterface {

  /**
   * Class constructor.
   */
  public function __construct(
    protected Client $httpClient,
    protected ConfigFactory $configFactory,
  ) {
    parent::__construct($httpClient, $configFactory);
    $this->apiBaseUrl = "$this->apiBaseUrl/ISteamNews";
  }

  /**
   * {@inheritdoc}
   */
  public function getNewsForApp(string $appid, int $count, string $feed_names = '') {
    if (empty($this->steamapikey)) {
      return;
    }

    $api_url = "$this->apiBaseUrl/GetNewsForApp/v0002/";
    $options = [
      'query' => [
        'key' => $this->steamapikey,
        'appid' => $appid,
        'count' => $count,
        'maxlength' => 300,
        'format' => 'json',
      ],
    ];
    if (!empty($feed_names)) {
      $options['feeds'] = $feed_names;
    }

    $response = $this->getResponse($api_url, $options);

    return $response['appnews']['newsitems'] ?? [];
  }

}
