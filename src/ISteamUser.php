<?php

namespace Drupal\steam_api;

use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;

/**
 * ISteamUser API call.
 */
class ISteamUser extends ISteamApiBase implements ISteamUserInterface {

  /**
   * Class constructor.
   */
  public function __construct(
    protected Client $httpClient,
    protected ConfigFactory $configFactory,
  ) {
    parent::__construct($httpClient, $configFactory);
    $this->apiBaseUrl = "$this->apiBaseUrl/ISteamUser";
  }

  /**
   * {@inheritdoc}
   */
  public function getFriendList(string $steamcommunity_id) {
    if (empty($this->steamapikey)) {
      return;
    }

    $api_url = "$this->apiBaseUrl/GetFriendList/v1/";
    $options = [
      'query' => [
        'key' => $this->steamapikey,
        'steamid' => $steamcommunity_id,
      ],
    ];

    $response = $this->getResponse($api_url, $options);

    return $response['friendslist']['friends'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPlayerBans(string $steamcommunity_ids) {
    if (empty($this->steamapikey)) {
      return;
    }

    $api_url = "$this->apiBaseUrl/GetPlayerBans/v1/";
    $options = [
      'query' => [
        'key' => $this->steamapikey,
        'steamids' => $steamcommunity_ids,
      ],
    ];

    $response = $this->getResponse($api_url, $options);

    return $response['response']['players'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPlayerSummaries(string $steamcommunity_ids) {
    if (empty($this->steamapikey)) {
      return;
    }

    $api_url = "$this->apiBaseUrl/GetPlayerSummaries/v0002/";
    $options = [
      'query' => [
        'key' => $this->steamapikey,
        'steamids' => $steamcommunity_ids,
      ],
    ];

    $response = $this->getResponse($api_url, $options);

    return $response['response']['players'] ?? [];
  }

}
